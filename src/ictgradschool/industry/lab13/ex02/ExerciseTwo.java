package ictgradschool.industry.lab13.ex02;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by qpen546 on 1/05/2017.
 */

    public class ExerciseTwo {
        private int value = 0;
        private void start() throws InterruptedException {
            List<Thread> threads = new ArrayList<>();
            for (int i = 1; i <= 100; i++) {
                /*for (Thread t : threads) {
                    t.join();
                }*/

                final int toAdd = i;
                Thread t = new Thread(new Runnable() {
                    @Override
                    public void run() {
                        add(toAdd);
                       /*System.out.println("index = " + toAdd);
                        System.out.println("value = " + value);
                        System.out.println("");*/
                    }
                });
                t.start();
                threads.add(t);
            }
            for (Thread t : threads) {
                t.join();
            }
            System.out.println("value = " + value);
        }
        private void add(int i) {
            value = value + i;
        }
        public static void main(String[] args) throws InterruptedException {
            new ExerciseTwo().start();
        }
    }


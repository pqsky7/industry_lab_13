package ictgradschool.industry.lab13.ex05;

import ictgradschool.Keyboard;

import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;

import static java.lang.Thread.interrupted;
import static java.lang.Thread.sleep;

/**
 * Created by pqsky on 2017/5/2.
 */
public class ExerciseFive {
    protected long startTime;
    protected long currentTime;
    protected long runTime;
    PrimeFactorsTask task;

    public void start() {

        Thread eventDispatch = new Thread(new Runnable() {
            @Override
            public void run() {
                while (!Thread.currentThread().isInterrupted()) {
                    long userInput;
                    List<Long> result = new ArrayList<>();
                    System.out.print("Please enter a number: ");
                    userInput = Long.parseLong(Keyboard.readInput());
                    task = new PrimeFactorsTask(userInput);

                    Thread computation = new Thread(task);


                    Thread abort = new Thread(new Runnable() {
                        @Override
                        public void run() {
                            System.out.println("Program is running...");
                            while(!Thread.currentThread().isInterrupted()) {
                                currentTime = System.currentTimeMillis();
                                runTime = currentTime - startTime;
                                if(runTime >= 3000) {
                                    System.out.println("Program will take long time to obtain the results");
                                    System.out.print("Type \"C\" to cancel or press any key to wait the result: ");
                                    if (Keyboard.readInput().toUpperCase().equals("C")) {
                                        computation.interrupt();
                                    }
                                   break;
                                }
                            }
                        }
                    });

                    computation.start();
                    startTime = System.currentTimeMillis();
                    abort.start();

                    try {
                        computation.join();
                        abort.interrupt();
                        abort.join();
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }

                    try {
                        result = task.getPrimeFactors();
                        System.out.print("The prime factors are:");
                        for (long element : result) {
                            System.out.print(" " + element);
                        }
                    } catch (IllegalStateException e) {
                        System.out.print("The task is canceled");
                    }

                    System.out.print("\nType \"E\" to exit or press Enter to continue: ");
                    if (Keyboard.readInput().toUpperCase().equals("E")) {
                        Thread.currentThread().interrupt();
                    }else{
                        System.out.println("");
                    }
                }
            }
        });
        eventDispatch.start();
    }

    public static void main(String[] args) {
        new ExerciseFive().start();
    }
}

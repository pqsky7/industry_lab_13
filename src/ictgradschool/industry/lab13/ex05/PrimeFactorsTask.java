package ictgradschool.industry.lab13.ex05;

import jdk.jfr.events.ExceptionThrownEvent;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by qpen546 on 2/05/2017.
 */
public class PrimeFactorsTask implements Runnable {
    private long n;
    private List<Long> primeNumList;

    public enum TaskState {
        Initialized, Completed, Aborted;
    }

    TaskState taskState;

    public PrimeFactorsTask(long n) {
        this.n = n;
        primeNumList = new ArrayList<>();
        taskState = TaskState.Initialized;
    }

    @Override
    public void run() {

        // for each potential factor
        for (long factor = 2; factor * factor <= n; factor++) {
            if(Thread.currentThread().isInterrupted()){
                taskState=TaskState.Aborted;
                break;
            }
            // if factor is a factor of n, repeatedly divide it out
            while (n % factor == 0) {
                primeNumList.add(factor);
                //System.out.print(factor + " ");
                n = n / factor;
            }
        }
        // if biggest factor occurs only once, n > 1
        if (n > 1) {
            primeNumList.add(n);
            //System.out.println(n);
        }

        if(taskState!=TaskState.Aborted) {
            taskState = TaskState.Completed;
        }
    }

    public long n() {
        return n;
    }

    public List<Long> getPrimeFactors() throws IllegalStateException {
        if (getState()!=TaskState.Completed){
            throw new IllegalStateException();
        }
        return primeNumList;
    }

    public TaskState getState() {
        return taskState;
    }

}

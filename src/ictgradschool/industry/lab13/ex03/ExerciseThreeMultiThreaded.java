package ictgradschool.industry.lab13.ex03;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ThreadLocalRandom;

/**
 * TODO Create a multi-threaded program to calculate PI using the Monte Carlo method.
 */
public class ExerciseThreeMultiThreaded extends ExerciseThreeSingleThreaded {
    long numInsideCircle = 0;
    /**
     * Estimates PI using a multi-threaded Monte Carlo method.
     */
    @Override
    protected double estimatePI(long numSamples) {
        // TODO Implement this.
        int threadNum = 20;
        long threadSamples = numSamples / threadNum;
        List<Thread> threads = new ArrayList<>();

        for (int i = 0; i < threadNum; i++) {
            final int index = i;
            long sample;

            Thread thread = new Thread(new Runnable() {
                @Override
                public void run() {

                    ThreadLocalRandom random = ThreadLocalRandom.current();
                    for (long j = 0; j < threadSamples; j++) {
                        double x = random.nextDouble();
                        double y = random.nextDouble();
                        if (Math.pow(x, 2.0) + Math.pow(y, 2.0) < 1.0) {
                            numInsideCircle++;
                        }
                    }
                }
            });
            thread.start();
            threads.add(thread);
        }

        for (Thread element : threads) {
            try {
                element.join();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

        return 4.0 * (double) numInsideCircle / (double) numSamples;
    }

    /**
     * Program entry point.
     */
    public static void main(String[] args) {
        new ExerciseThreeMultiThreaded().start();
    }
}

package ictgradschool.industry.lab13.ex04;

import java.util.List;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.TimeUnit;

/**
 * Created by qpen546 on 1/05/2017.
 */
public class Consumer implements Runnable {
    protected BankAccount account;
    protected BlockingQueue<Transaction> queue;


    public Consumer(BankAccount account, BlockingQueue<Transaction> queue) {
        this.account = account;
        this.queue = queue;
    }

    @Override
    public void run() {
        // TODO Stuff & things
        // Acquire Transactions to process.
        try {
            while (!Thread.currentThread().isInterrupted()) {
                Transaction transaction = null;
                transaction = queue.take();
                if (transaction != null) {
                    switch (transaction._type) {
                        case Deposit:
                            account.deposit(transaction._amountInCents);
                            break;
                        case Withdraw:
                            account.withdraw(transaction._amountInCents);
                            break;
                    }
                }
            }
        } catch (InterruptedException e) {
            e.getCause();
        }
    }
}

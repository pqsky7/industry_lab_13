package ictgradschool.industry.lab13.ex04;

import java.util.List;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;

/**
 * Created by qpen546 on 1/05/2017.
 */
public class ConcurrentBankingApp {
    public void start() {
        BlockingQueue<Transaction> queue = new ArrayBlockingQueue<>(10);
        List<Transaction> transactions = TransactionGenerator.readDataFile();
        BankAccount account = new BankAccount();

        Thread producer = new Thread(new Runnable() {
            @Override
            public void run() {
                // TODO Stuff & things
                for (Transaction element : transactions) {
                    try {
                        queue.put(element);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }
        });
        Thread consumerOne = new Thread(new Consumer(account, queue));
        Thread consumerTwo = new Thread(new Consumer(account, queue));
        Thread check = new Thread(new Runnable() {
            @Override
            public void run() {
                while (!queue.isEmpty()) {
                    try {
                        wait(200);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }
        });

        producer.start();
        consumerOne.start();
        consumerTwo.start();
        try {
            producer.join();

            while(!queue.isEmpty());

            consumerOne.interrupt();
            consumerTwo.interrupt();

            consumerOne.join();
            consumerTwo.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }


        System.out.println("Final balance: " + account.getFormattedBalance());
    }

    public static void main(String[] args) {
        new ConcurrentBankingApp().start();
    }
}

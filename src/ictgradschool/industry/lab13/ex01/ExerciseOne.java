package ictgradschool.industry.lab13.ex01;

import ictgradschool.Keyboard;

/**
 * Created by qpen546 on 1/05/2017.
 */
public class ExerciseOne {
    public void start() {
        Runnable myRunnable = new Runnable() {
            int counter = 0;
            @Override
            public void run() {
                while(!Thread.currentThread().isInterrupted()) {
                    counter++;
                }
                System.out.print("Counter: "+ counter);
            }

        };
        Thread thread = new Thread(myRunnable);
        thread.start();
        System.out.println("start thread...");
        System.out.println("press Enter to stop tread");
        Keyboard.readInput();
        thread.interrupt();

        try{
            thread.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public static void main(String[] arg){
        new ExerciseOne().start();
    }
}

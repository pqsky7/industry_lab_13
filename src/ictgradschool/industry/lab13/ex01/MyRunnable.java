package ictgradschool.industry.lab13.ex01;

/**
 * Created by qpen546 on 1/05/2017.
 */

public class MyRunnable implements Runnable {
    private long counter = 0;
    public void run(){
        while(!Thread.currentThread().isInterrupted()) {
            counter++;
        }
    }

    public void printCounter(){
        System.out.print("Counter: "+counter);
    }
}
